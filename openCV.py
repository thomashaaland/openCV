import numpy as np
import cv2 as cv
import time
import random
from collections import deque
from functools import reduce

def difference(pFrames):
    return cv.subtract(pFrames[0], pFrames[-1])

def colorReplace(frame):
    hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
    lower_red = np.array([0, 40, 45])
    upper_red = np.array([55, 255, 225])
    mask = cv.inRange(hsv, lower_red, upper_red)
    res = cv.colorChange(frame, mask, red_mul=0, green_mul=2, blue_mul=5)
    return res

def blend(pFrames):
    original = pFrames[0]
    for frame in pFrames:
        dst = cv.addWeighted(original, 0.6, frame, 0.4, 0)
    return dst

def gradualBlend(frame1, frame2, blend):
    opaque = blend/100 if blend < 100 else 1
    return cv.addWeighted(frame1, opaque, frame2, 1 - opaque, 0)

def weirdMirror(frame):
    ltemp = frame[:,320:,:]
    frame = cv.flip(frame,1)
    frame[:,320:,:] = ltemp

    return frame

def drawOn(frame, text):
    font = cv.FONT_HERSHEY_COMPLEX
    result = cv.putText(frame, text, (10, 100), font, 2.3, (255, 255, 255), 2, cv.LINE_AA)
    return cv.putText(result, text, (11, 101), font, 2.3, (0, 0, 0), 2, cv.LINE_AA)

nv = 8
nh = 12
vSample = random.sample(list(range(nv)), k = nv)
hSample = random.sample(list(range(nh)), k = nh)

def checkers(frame):
    vInt = frame.shape[0]//nv
    hInt = frame.shape[1]//nh
    original = frame.copy()
    pieces = [[frame[vInt*i:vInt*(i+1), hInt*j:hInt*(j+1),:] for i in range(nv)] for j in range(nh)]
    for i, ii in enumerate(vSample):
        for j, jj in enumerate(hSample):
            original[vInt*i:vInt*(i+1), hInt*j:hInt*(j+1),:] = pieces[jj][ii]
    return original

def drawSmiley(frame):
    img = cv.circle(frame, (200,200), 100, (3, 186, 252), -1)
    img = cv.circle(img, (190,190), 80, (3, 236, 252), -1)
    img = cv.circle(img, (160,160), 30, (189, 251, 255), -1)
    img = cv.circle(img, (160,180), 30, (0, 0, 0), -1)
    img = cv.circle(img, (150,165), 8, (255, 255, 255), -1)
    img = cv.circle(img, (230,180), 28, (0, 0, 0), -1)
    img = cv.circle(img, (220,165), 8, (255, 255, 255), -1)

    # Polyline
    pts = np.array([[120, 220], [160, 240], [200, 250], [240, 240], [280, 220], [240, 270], [200, 280], [160, 270]], np.int32)
    img = cv.fillPoly(img, [pts], (0, 0, 0))

    img = cv.circle(img, (200,200), 100, (255, 0, 0))
    return img


def main():
    start = time.time()
    cap = cv.VideoCapture(0)
    pFrames = deque(maxlen = 50)
    opaque = 0

    fourcc = cv.VideoWriter_fourcc(*'XVID')
    out = cv.VideoWriter('output.avi', fourcc, 80.0, (640, 480))

    if not cap.isOpened():
        print("Cannot open camera")
        exit()
    while True:
        end = time.time()
        ret, frame = cap.read()

        if not ret:
            print("Can't recieve frame. Exiting")
            break
        gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        pFrames.append(gray)

        if end - start < 10:
            display = drawOn(frame, "Original")
        elif end - start < 20:
            display = drawOn(checkers(frame), "Checkers")
        elif end - start < 30:
            display = drawOn(gray, "Grayscale")
        elif end - start < 40:
            display = drawOn(difference(pFrames), "Motion Detection")
        elif end - start < 50:
            display = drawOn(colorReplace(frame), "Color Replace")
        elif end - start < 60:
            display = drawOn(blend(pFrames), "Ghost Motion")
        elif end - start < 70:
            display = drawOn(weirdMirror(frame), "Mirroring")
        elif end - start < 80:
            opaque += 1
            display = drawOn(gradualBlend(drawSmiley(frame.copy()), frame, opaque), "Finished")
        else:
            break

        out.write(display)
        cv.imshow('frame', display)

        if cv.waitKey(1) == ord('q'):
            break
    
    cap.release()
    out.release()
    cv.destroyAllWindows()



if __name__ == "__main__":
    main()