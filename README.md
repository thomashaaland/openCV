# Open CV experimentation

This project demonstrates some of the functionality of OpenCV. The python script records from the camera installed on your computer if there is one present. It fails otherwise. This image is distorted in several ways such as color replacing, shuffling, mirroring, motion detecting, grayscale and more.

## Install
To run the script you need to have Python3 installed. If you are on windows get Python3 from windows store or use (Anaconda)[www.anaconda.com]. On linux Python3 should come with the basic system installation. It relies on the packages OpenCv and Numpy. To install these type `pip install numpy` and `pip install opencv-python` in a terminal.

## Usage
To run the script type `python3 openCV.py` in a terminal or Cmd. Once finished the script reveals all the solutions for an 8x8 board.

## Contributing
Send questions and suggestions to <thomas.haaland@gmail.com>

UNLICENSED
